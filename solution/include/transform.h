#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "image.h"
#include <stdio.h>

size_t rotate_90_left(const size_t row, const size_t column, const uint64_t width);

struct image rotate(struct image const source, size_t (*rot_ptr)(size_t, size_t, uint64_t));

#endif // TRANSFORM_H
