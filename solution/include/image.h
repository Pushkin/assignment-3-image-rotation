#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


struct pixel {
    uint8_t b, g, r;
};

struct __attribute__((packed)) image {
    uint64_t width, height;
    struct pixel *data;
};

struct image struct_image_create(uint64_t width, uint64_t height);

void struct_image_destroy(struct image img);

#endif //IMAGE_H
