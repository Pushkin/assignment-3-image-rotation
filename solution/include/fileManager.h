#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include <stdio.h>

int open_file(FILE** const file, char* const path, char* const mode);

int close_file(FILE* const file);

#endif //FILE_MANAGER_H
