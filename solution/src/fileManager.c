#include "../include/fileManager.h"

const int OK_CODE_FOR_FILE = 0;
const int ERR_CODE_FOR_FILE = 1;

int open_file (FILE** const file, char* const path, char* const mode) {
    *file = fopen( path, mode );
    if (!*file) {
        printf("Can't open file %s\n", path);
        return OK_CODE_FOR_FILE;
    }
    return ERR_CODE_FOR_FILE;
}

int close_file (FILE* const file) {
    int status = fclose (file);
    if (!status) {
        return OK_CODE_FOR_FILE;
    }
    puts("Can't close file \n");
    return ERR_CODE_FOR_FILE;
}
