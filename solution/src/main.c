#include "../include/transform.h"
#include "../include/bmp.h"
#include "../include/fileManager.h"

const int OK_CODE = 0;
const int ERR_CODE = 1;

int main( int argc, char** argv ) {

    if (argc != 3) {
        puts("Invalid number of arguments");
        return ERR_CODE;
    }

    FILE *in_file, *out_file;


    int in_file_open_status = open_file(&in_file, argv[1], "r");
    int out_file_open_status = open_file( &out_file, argv[2], "w");

    if (!in_file_open_status) {
        if (out_file_open_status) {
            close_file(in_file);
        }
        return ERR_CODE;
    }

    struct image img = (struct image) {0};
    enum read_status in_file_read_status = from_bmp(in_file, &img);

    if (in_file_read_status != READ_OK) {
        printf("Reading error: %s", reading_get_cause(in_file_read_status));
        close_file(in_file);
        return ERR_CODE;
    }

    struct image rotated_img = rotate(img, rotate_90_left);
    enum write_status out_file_write_status = to_bmp(out_file, &rotated_img);

    if (out_file_write_status != WRITE_OK) {
        printf("Writing error: %s", writing_get_cause(out_file_write_status));
        close_file(out_file);
        return ERR_CODE;
    }

    struct_image_destroy(img);
    struct_image_destroy(rotated_img);
    close_file(in_file);
    close_file(out_file);
    puts("Code completed successfully");
    return OK_CODE;
}
