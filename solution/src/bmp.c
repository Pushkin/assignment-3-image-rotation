#include "../include/bmp.h"

const uint16_t VALID_BMP_TYPE_1 = 0x4d42;
const uint16_t VALID_BMP_TYPE_2 = 0x4349;
const uint16_t VALID_BMP_TYPE_3 = 0x5450;

const uint32_t BMP_OFF_BITS = sizeof(struct bmp_header);
const uint32_t BMP_IMAGE_SIZE = 40;

const uint16_t BMP_IMAGE_PLANES = 3;
const uint16_t BMP_IMAGE_BITCOUNT = 24;
const uint32_t BMP_IMAGE_COMPRESSION = 0;

const char* const read_err_msg[] = {
        [READ_OK] = "the reading was successful",
        [READ_INVALID_SIGNATURE] = "the signature turned out to be bad",
        [READ_INVALID_BITS] = "the bits turned out to be bad",
        [READ_INVALID_HEADER] = "the header turned out to be bad"
};

const char* const write_err_msg[] = {
        [WRITE_OK] = "the writing was successful",
        [WRITE_ERROR] = "the writing went wrong"
};

int64_t count_padding(uint64_t const width) {
    return ((((int64_t) width) * 3 + 3) & (-4)) - (((int64_t) width) * 3);
}

struct bmp_header file_params(struct image const* img, uint64_t count_byte_padding, uint16_t bf_type, uint16_t bf_offtop_bits,
                              uint64_t bf_img_size, uint64_t bmp_planes,uint64_t bmp_bitcount,uint64_t bmp_compression) {
    return (struct bmp_header) {
            .bfType = bf_type,
            .bOffBits = bf_offtop_bits,
            .biSize = bf_img_size,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = bmp_planes,
            .biBitCount = bmp_bitcount,
            .biCompression = bmp_compression,
            .biSizeImage = (img->width * sizeof(struct pixel) + count_byte_padding) * img->height,
            .bfileSize = (img->width * sizeof(struct pixel) + count_byte_padding) * img->height + sizeof(struct bmp_header)
    };
}

const char* reading_get_cause(enum read_status status) {
    return read_err_msg[status];
}

const char* writing_get_cause(enum write_status status) {
    return write_err_msg[status];
}

enum read_status from_bmp(FILE* const in, struct image* img) {
    struct bmp_header file_header;

    if (!fread(&file_header, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_HEADER;
    }

    if (file_header.bfType != VALID_BMP_TYPE_1 && file_header.bfType != VALID_BMP_TYPE_2 && file_header.bfType != VALID_BMP_TYPE_3) {
        return READ_INVALID_SIGNATURE;
    }

    if (file_header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    *img = struct_image_create(file_header.biWidth, file_header.biHeight);

    int64_t count_byte_padding = count_padding(img->width);

    for (uint64_t i = 0; i < img->height; i++) {
        if (fread(img->data + img->width * i, sizeof(struct pixel), img->width, in) != img->width) {
            return READ_ERROR;
        }
        if (fseek(in, count_byte_padding, SEEK_CUR )) {
            return READ_ERROR;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    int64_t count_byte_padding = count_padding(img->width);
    struct bmp_header file_header = file_params(img, count_byte_padding, VALID_BMP_TYPE_1, BMP_OFF_BITS,
          BMP_IMAGE_SIZE, BMP_IMAGE_PLANES, BMP_IMAGE_BITCOUNT, BMP_IMAGE_COMPRESSION);

    if (!fwrite(&file_header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    struct pixel *garbage_pixels = img->data;

    for (uint64_t i = 0; i < img->height; i++) {
        if (!fwrite(img->data + i*img->width, sizeof(struct pixel) * img->width, 1, out)) {
            return WRITE_ERROR;
        }
        if (!fwrite(garbage_pixels, count_byte_padding, 1, out) && count_byte_padding) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
