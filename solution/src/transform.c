#include "../include/transform.h"

#include <stdlib.h>


size_t rotate_90_left(const size_t row, const size_t column, const uint64_t width) {
    return row * width + column;
}

struct image rotate(struct image const source, size_t (*rot_ptr)(size_t, size_t, uint64_t)) {
    struct image new_image = struct_image_create(source.height, source.width);
    if (!new_image.data) {
        return new_image;
    }
    for (size_t row = 0; row < new_image.height; row++) {
        for (size_t column = 0; column < new_image.width; column++) {
            size_t old_index = rot_ptr(source.height - 1 - column, row, source.width);
            size_t new_index = rot_ptr(row, column, new_image.width);
            new_image.data[new_index] = source.data[old_index];
        }
    }
    return new_image;
}
