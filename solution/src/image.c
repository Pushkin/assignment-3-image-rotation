#include "../include/image.h"

#include<stdlib.h>

struct image struct_image_create(uint64_t width, uint64_t height) {
    struct image img = {.width = width, .height = height};
    img.data = malloc(img.width * img.height * sizeof(struct pixel));
    return img;
}

void struct_image_destroy(struct image img) {
    if(img.data)
        free(img.data);
}

